#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char* prefix(char *source, size_t length) {
  size_t size = length + 1;
  char *substring = (char *) malloc(size);
  memset(substring, 0, size);
  strncpy(substring, source, length);
  return substring;
}

int main(int argc, char *argv[]) {
  int pairs = (argc - 1) / 3;
  clock_t start = clock();
  for (int i = 0; i < pairs; i++) {
    //printf("%d\n", i);
    int index = 1 + i * 3;
    char* lhs = argv[index];
    char* rhs = argv[index + 2];
    //printf("%s\n", lhs);
    //printf("%s\n", rhs);
    size_t size = strlen(lhs) + strlen(rhs) + 1;
    char *output = (char *) malloc(size);
    memset(output, 0, size);
    for (int x = 0; x < strlen(lhs) - 2; x += 2) {
      //printf("%d\n", x);
      if (x == 0) {
        char *rhsPrefix = prefix(rhs, strlen(lhs));
        if (strcmp(lhs, rhsPrefix) == 0) {
          free(rhsPrefix);
          printf("%s\n", rhs);
          break;
        }
        free(rhsPrefix);
      }
      char *lhsSubstring = prefix(lhs, x);
      sprintf(output, "%s%s", lhsSubstring, rhs);
      free(lhsSubstring);
      //printf("output: %s\n", output);

      char *check = prefix(output, strlen(lhs));
      //printf("check: %s\n", check);
      //printf("lhs: %s\n", lhs);
      if (strcmp(lhs, check) == 0) {
        free(check);
        printf("%s\n", output);
        break;
      }
      free(check);
    }
    free(output);
  }
  double time = (double)(clock() - start) / CLOCKS_PER_SEC;
  printf("Perform time: %f\n", time);
  //printf("%d\n", pairs);
};
