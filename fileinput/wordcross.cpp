#include <iostream>
#include <fstream>
#include <time.h>
#include <pcre.h>
#include <string.h>
using namespace std;

#define kOUTPUT_ENABLED 0

int main(int argc, char *argv[]) {
  ifstream file("vxod.txt");
  ofstream outputFile("vyxod.txt");

  string linePP;
  const char* prce_error;
  int pcre_erroff;
  int match[9] = {0};
  char buffer[256] = {0};

  clock_t start = clock();
  auto regex = pcre_compile("([^+ ]*?)([^+ ]*)[ +]+\\2", PCRE_UTF8, &prce_error, &pcre_erroff, 0);
  while (getline(file, linePP)) {
    auto line = linePP.c_str();
    auto rc = pcre_exec(regex, 0, line, strlen(line), 0, PCRE_ANCHORED, match, 9);
    int s2 = match[2*1];
    int s1 = match[2*1+1] - match[2*1];
    int s4 = match[2*2];
    int s3 = match[2*2+1] - match[2*2];
    int start = s4 + s3 + 3;
    int length = strlen(line) - start;
    sprintf(buffer, "%.*s%.*s\n",s1, line + s2, length, line + start);
    #if kOUTPUT_ENABLED
    cout << buffer;
    #endif
    outputFile << buffer;
  }
  double time = (double)(clock() - start) / CLOCKS_PER_SEC;
  cout << "Perform time: " << time << " seconds "<< endl;
};
