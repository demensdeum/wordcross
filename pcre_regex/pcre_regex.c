#include <pcre.h>
#include <string.h>
#include <stdio.h>

#define kPRINT_MATCH 0

const char* line = "очень длинная строка в которой мы ищем слова";
const char* pattern = "которой";
const int groups = 1;

int main(int argc, char **argv) {
  const int matchSize = groups * 3; // Manual: The number passed in ovecsize should always be a multiple of three
  const char* prce_error;
  int pcre_erroff;
  int match[matchSize] = {0};
  pcre* regex = pcre_compile(pattern, PCRE_UTF8, &prce_error, &pcre_erroff, 0);
  int rc = pcre_exec(regex, 0, line, strlen(line), 0, 0, match, matchSize);
#if kPRINT_MATCH
  for (int i = 0; i < matchSize; i++) {
    printf("%d,", match[i]);
  }
  printf("\n");
#endif
  for (int i = 0; i < groups; i++) {
    int start = match[i*2];
    int end = match[i*2 + 1];
    printf("%.*s\n", end - start, line + start);
  }
};
