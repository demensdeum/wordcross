#include <iostream>
#include <regex>
#include <time.h>
using namespace std;
int main(int argc, char *argv[]) {
  int pairs = (argc - 1) / 3;
  clock_t start = clock();
  for (int i = 0; i < pairs; i++) {
    int index = 1 + i * 3;
    auto lhs = string(argv[index]);
    auto rhs = string(argv[index + 2]);
    string pair = "";
    pair += lhs;
    pair += " + ";
    pair += rhs;
    auto output = regex_replace(pair, regex(R"(([^+ ]*?)([^+ ]*)[ +]+\2)"), "$1$2");
    if (output.length() >= lhs.length() + rhs.length()) {
      continue;
    }
    auto check = output.substr(0, lhs.length());
    if (lhs.compare(check) == 0) {
      cout << output << endl;
    }
  }
  double time = (double)(clock() - start) / CLOCKS_PER_SEC;
  cout << "Perform time: " << time << endl;
};
