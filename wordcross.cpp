#include <iostream>
#include <time.h>
using namespace std;
int main(int argc, char *argv[]) {
  int pairs = (argc - 1) / 3;
  cout << pairs << endl;
  clock_t start = clock();
  for (int i = 0; i < pairs; i++) {
    //cout << i << endl;
    int index = 1 + i * 3;
    auto lhs = string(argv[index]);
    auto rhs = string(argv[index + 2]);
    //cout << "LHS: " << lhs << endl;
    //cout << "RHS: " << rhs << endl;
    for (int x = 0; x < lhs.length() - 2; x += 2) {
      if (x == 0) {
        if (lhs.compare(rhs.substr(0, lhs.length())) == 0) {
          cout << rhs << endl;
          break;
        }
      }
      //cout << x << endl;
      string output = "";
      output += lhs.substr(0, x + 2);
      output += rhs;
      //cout << output << endl;
      auto check = output.substr(0, lhs.length());
      //cout << "CHECK: " << check << endl;
      if (lhs.compare(check) == 0) {
        cout << output << endl;
        break;
      }
    }
  }
  double time = (double)(clock() - start) / CLOCKS_PER_SEC;
  cout << "Perform time: " << time << endl;
};
