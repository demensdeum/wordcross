#include <iostream>
#include <fstream>
#include <time.h>
#include <pcre.h>
#include <string.h>
using namespace std;

#define kPRINT_MATCH 0

const char* line = "foobar";
const char* pattern = "foo(?=bar)";

const int groups = 1;
const int matchSize = groups * 2;

int main(int argc, char *argv[]) {
  const char* prce_error;
  int pcre_erroff;
  int match[matchSize] = {0};
  auto regex = pcre_compile(pattern, PCRE_UTF8, &prce_error, &pcre_erroff, 0);
  auto rc = pcre_exec(regex, 0, line, strlen(line), 0, 0, match, matchSize);
#if kPRINT_MATCH
  for (int i = 0; i < matchSize; i++) {
    printf("%d,", match[i]);
  }
#endif
  int start = match[0];
  int end = match[1];
  printf("%.*s\n", end, line + start);
};
